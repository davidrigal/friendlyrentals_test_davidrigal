﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace fr_stringcalculator
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Title = "Friendlyrentals string calculator kata";
            Console.WriteLine("Follow instructions on 'readme.md'");
            Console.WriteLine("empty string: " + StringCalculator.Add(""));
            Console.WriteLine("'1' string: " + StringCalculator.Add("1"));
            Console.WriteLine("'1,2'empty string: " + StringCalculator.Add("1,2"));
            Console.WriteLine("'1,2\n3,4,5'empty string: " + StringCalculator.Add("1,2,3,4,5"));
            Console.WriteLine("'//,ot\n1,2o3,4t5'empty string: " + StringCalculator.Add("//,ot\n1,2o3,4t5"));
            Console.WriteLine("'1,2\n3,4,5'empty string: " + StringCalculator.Add("1,-2,-3,4,-5"));
            Console.ReadLine();
        }
    }

    static class StringCalculator
    {
        public static Int32 Add(String strNumbers)
        {
            Int32 _return;
            Int32 _int_separators_lenght;
            Int32 _parsed_value;
            Char[] _separators;
            String[] _parsed_number;
            String _str_separator;
            String _error_message;

            _return = 0;
            _error_message = "";

            if (strNumbers.Length == 0)
            {
                return _return;
            }

            if (strNumbers.Length > 1 && strNumbers.Substring(0, 2) == "//")
            {
                _str_separator = strNumbers.Split('\n')[0];
                _int_separators_lenght = _str_separator.Length;
                _separators = _str_separator.Substring(2).ToCharArray();
                strNumbers = strNumbers.Substring(_int_separators_lenght);
            }
            else
            {
                _separators = ",\n".ToCharArray();
            }

            _parsed_number = strNumbers.Split(_separators);

            foreach (String _str_number in _parsed_number)
            {
                _parsed_value = Int32.Parse(_str_number);
                if(_parsed_value < 0)
                {
                    _error_message += _str_number + ", ";
                }
                else
                {
                    _return += _parsed_value;
                }
            }

            if(_error_message.Length > 0)
            {
                throw new Exception("Negatives not allowed: '" + _error_message.Substring(0, _error_message.Length - 2));
            }

            return _return;
        }
    }
}
